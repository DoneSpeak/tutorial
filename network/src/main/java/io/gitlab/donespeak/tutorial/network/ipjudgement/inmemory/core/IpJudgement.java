package io.gitlab.donespeak.tutorial.network.ipjudgement.inmemory.core;

/**
 * @author Yang Guanrong
 * @date 2020/03/28 15:07
 */
public interface IpJudgement {
	boolean isIncluded(String ip);
	IpRange getIncluded(String ip);
}
