package io;

import org.apache.commons.io.IOUtils;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

/**
 * @author donespeak
 */
public class InputStreamTest {

    public void available() throws IOException {
        OutputStream outputStream = new ByteArrayOutputStream();
        InputStream inputStream = new ByteArrayInputStream("userapp".getBytes());
        int count = inputStream.available();

        IOUtils.copy(inputStream, outputStream);
    }
}
