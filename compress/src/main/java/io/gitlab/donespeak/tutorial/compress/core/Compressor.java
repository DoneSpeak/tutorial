package io.gitlab.donespeak.tutorial.compress.core;

import java.io.File;

/**
 * @author DoneSpeak
 */
public interface Compressor {

    File compress(File originalFile);

    File uncompress(File compressedFile);

    void compress(File originalFile, File compressedFile);

    void uncompress(File compressedFile, File originalFile);
}
