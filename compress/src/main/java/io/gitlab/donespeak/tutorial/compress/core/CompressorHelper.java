package io.gitlab.donespeak.tutorial.compress.core;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

/**
 * @author DoneSpeak
 */
public class CompressorHelper extends FileHelper {

    private List<Class<? extends Compressor>> compressChain;
    private boolean needCompressed;

    public CompressorHelper() {
        compressChain = new ArrayList<>();
    }

    public CompressorHelper with(Class<? extends Compressor> compressorType) {
        compressChain.add(compressorType);
        return this;
    }

    public CompressorHelper with(CompressType type) {
        compressChain.add(getCompressorByType(type));
        return this;
    }

    private Class<? extends Compressor> getCompressorByType(CompressType type) {
        // TODO
        return null;
    }

    public void compress() throws Exception {
        this.needCompressed = true;
        File sourceFile = sourceFile();
        File targetFile = targetFile();;
        for(Compressor compressor: getCompressors()) {
            compressor.compress(sourceFile, targetFile);
            sourceFile = targetFile;
        }
    }

    public void uncompress() throws Exception {
        this.needCompressed = false;
        File sourceFile = sourceFile();
        File targetFile = targetFile();;
        for(Compressor compressor: getCompressors()) {
            compressor.compress(sourceFile, targetFile);
            sourceFile = targetFile;
        }
    }

    private List<Compressor> getCompressors() throws ReflectiveOperationException {
        List<Class<? extends Compressor>> compressTypes = getCompressTypes();
        List<Compressor> compressors = new ArrayList<>(compressTypes.size());
        for(Class<? extends Compressor> clazz: compressTypes) {
            compressors.add(clazz.newInstance());
        }
        return compressors;
    }

    private List<Class<? extends Compressor>> getCompressTypes() {
        List<Class<? extends Compressor>> compressTypes = new ArrayList<>(compressChain);
        if(compressTypes.isEmpty()) {
            compressTypes = guessCompressTypeFromFileExtension();
        }
        return compressTypes;
    }

    private List<Class<? extends Compressor>> guessCompressTypeFromFileExtension() {
        File guessByFile = this.needCompressed? targetFile(): sourceFile();
        // TODO
        return null;
    }
}
