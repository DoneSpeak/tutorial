package io.gitlab.donespeak.tutorial.retryer.guavaretryer;

import com.github.rholder.retry.RetryException;
import com.github.rholder.retry.Retryer;
import com.github.rholder.retry.RetryerBuilder;
import com.github.rholder.retry.StopStrategies;
import com.github.rholder.retry.WaitStrategies;
import com.google.common.collect.ImmutableMap;
import com.sun.javafx.binding.StringFormatter;
import org.junit.Test;

import java.time.LocalDateTime;
import java.util.Map;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * @author donespeak
 */
public class ExceptionTester {

    @Test
    public void test() {
        AtomicInteger count = new AtomicInteger(0);
        Retryer<Integer> retryer = newCommonRetryer();
        try {
            Integer result = retryer.call(() -> {
                System.out.println(LocalDateTime.now() + ", run times: " + count.incrementAndGet());
                int val = 1 / 0;
                return val;
            });
            System.out.println(LocalDateTime.now() + ", Result is :" + result);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    public void testBoolean() throws ExecutionException, RetryException {
        AtomicInteger count = new AtomicInteger(0);
        Retryer<Boolean> retryer = newCommonRetryer();
        Boolean result = retryer.call(() -> {
            System.out.println(LocalDateTime.now() + ", run times: " + count.incrementAndGet());
            return true;
        });
        System.out.println(LocalDateTime.now() + ", Result is :" + result);
    }

    public <T> Retryer<T> newCommonRetryer() {
        // 尽量重试，确保程序正确执行
        Retryer<T> retryer = RetryerBuilder.<T>newBuilder()
//            .retryIfExceptionOfType(ArithmeticException.class)
            .retryIfExceptionOfType(Throwable.class)
            .withWaitStrategy(WaitStrategies.exponentialWait(200, 3, TimeUnit.SECONDS))
            .withStopStrategy(StopStrategies.stopAfterAttempt(5))
            .build();
        return retryer;
    }

    @Test
    public void testMap() {
        Map<String, String> map = ImmutableMap.of("jha", "val");
        String message = "map: %s";
        message = String.format(message, map);
        System.out.println(message);
    }
}
