package io.gitlab.donespeak.tutorial.excel.data.repository;

import com.github.jsonzou.jmockdata.JMockData;
import com.github.jsonzou.jmockdata.TypeReference;

import java.util.ArrayList;
import java.util.List;

/**
 * @author DoneSpeak
 * @date 2019/11/14 14:37
 */
public abstract class AbstractRepository <T> {

    private final Class<T> clazz;

    public AbstractRepository(Class<T> clazz) {
        this.clazz = clazz;
    }

    public T getOne() {
        return JMockData.mock(clazz);
    }

    public List<T> getList() {
        List<T> list = JMockData.mock(new TypeReference<List<T>>(){});
        return list;
    }

    public List<T> getList(int num) {
        List<T> list = new ArrayList<>(num);
        while(num > 0) {
            list.add(getOne());
            num --;
        }
        return list;
    }
}
