package io.gitlab.donespeak.tutorial.excel.easyexcel.timezone;

import com.alibaba.excel.converters.date.DateNumberConverter;
import com.alibaba.excel.metadata.CellData;
import com.alibaba.excel.metadata.GlobalConfiguration;
import com.alibaba.excel.metadata.property.ExcelContentProperty;
import lombok.extern.slf4j.Slf4j;
import org.apache.poi.ss.usermodel.DateUtil;

import java.math.BigDecimal;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

/**
 * @author DoneSpeak
 * @date 2019/11/21 22:01
 */
@Slf4j
public class DateTimeZoneNumberConverter extends DateNumberConverter {

    private final String globalTimeZoneId;

    public DateTimeZoneNumberConverter() {
        this(null);
    }

    public DateTimeZoneNumberConverter(String timeZoneId) {
        super();
        this.globalTimeZoneId = timeZoneId;
    }

    @Override
    public Date convertToJavaData(CellData cellData, ExcelContentProperty contentProperty,
        GlobalConfiguration globalConfiguration) {

        TimeZone timeZone = getTimeZone(contentProperty);
        boolean use1904windowing = getUse1904windowing(contentProperty, globalConfiguration);

        return DateUtil.getJavaDate(cellData.getNumberValue().doubleValue(), use1904windowing, timeZone);
    }

    @Override
    public CellData convertToExcelData(Date value, ExcelContentProperty contentProperty,
        GlobalConfiguration globalConfiguration) {

        TimeZone timeZone = getTimeZone(contentProperty);
        Calendar calendar = getCalendar(value, timeZone);

        boolean use1904windowing = getUse1904windowing(contentProperty, globalConfiguration);

        CellData cellData = new CellData(BigDecimal.valueOf(DateUtil.getExcelDate(calendar, use1904windowing)));

        return cellData;
    }

    private TimeZone getTimeZone(ExcelContentProperty contentProperty) {
        if(contentProperty == null) {
            return null;
        }
        String timeZoneId = DateTimeZoneUtil.getTimeZone(contentProperty.getField(), globalTimeZoneId);
        return TimeZone.getTimeZone(timeZoneId);
    }

    private Calendar getCalendar(Date date, TimeZone timeZone) {
        Calendar calStart = Calendar.getInstance();
        calStart.setTime(date);
        if(timeZone != null) {
            calStart.setTimeZone(timeZone);
        }

        return calStart;
    }

    private boolean getUse1904windowing(ExcelContentProperty contentProperty, GlobalConfiguration globalConfiguration) {
        if (contentProperty == null || contentProperty.getDateTimeFormatProperty() == null) {
            return contentProperty.getDateTimeFormatProperty().getUse1904windowing();
        } else {
            return globalConfiguration.getUse1904windowing();
        }
    }
}
