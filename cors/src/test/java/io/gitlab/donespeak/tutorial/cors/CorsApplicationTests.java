package io.gitlab.donespeak.tutorial.cors;

import org.junit.Test;
import org.springframework.boot.test.context.SpringBootTest;

import java.io.File;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;

@SpringBootTest
public class CorsApplicationTests {

    @Test
    public void contextLoads() {
        String strDate2 = "2021-07-27 00:00:00";
        DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
        dateTimeFormatter.withZone(ZoneId.of("US/Central"));
        LocalDateTime dateTime = LocalDateTime.parse(strDate2, dateTimeFormatter);
        System.out.println(dateTime.atZone(ZoneId.of("US/Central")).toInstant().toEpochMilli());
    }

    @Test
    public void test() {
        File file = new File("/Volumes/Seagate/E.zip");
        System.out.println(file.length());
    }
}

