package io.gitlab.donespeak.tutorial.cors.entity;

import lombok.Data;

/**
 * @date 2019/12/02 21:14
 */
@Data
public class User {
    private long userId;
    private String name;
    private int age;
}
