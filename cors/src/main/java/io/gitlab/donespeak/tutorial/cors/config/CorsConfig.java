package io.gitlab.donespeak.tutorial.cors.config;

import io.gitlab.donespeak.tutorial.cors.config.properties.CorsProperties;
import io.gitlab.donespeak.tutorial.cors.config.properties.CorsPropertiesList;
import io.gitlab.donespeak.tutorial.cors.config.support.AntPathMatcherCorsConfiguration;
import io.gitlab.donespeak.tutorial.cors.filter.CustomCorsFilter;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.CorsConfigurationSource;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;
import org.springframework.web.filter.CorsFilter;
import org.springframework.web.servlet.config.annotation.CorsRegistration;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import java.util.Arrays;

/**
 * @author Yang Guanrong
 * @date 2019/12/02 21:39
 */
@EnableConfigurationProperties
@Configuration
public class CorsConfig {

    @Configuration
    @ConditionalOnProperty(prefix = "web.config", name = "cors", havingValue = "sourceConfig")
    public static class CorsConfigurationSourceConfig {

        @Bean
        public CorsConfigurationSource corsConfigurationSource(CorsPropertiesList corsPropertiesList) {
            System.out.println("init bean CorsConfigurationSource with " + corsPropertiesList);
            return createCorsConfigurationSource(corsPropertiesList);
        }
    }

    /**
     * 不可与 @CrossOrigin 联用
     */
    @Configuration
    @ConditionalOnProperty(prefix = "web.config", name = "cors", havingValue = "corsFilterRegistration")
    public static class CorsFilterRegistrationConfig {

        @Bean
        public FilterRegistrationBean corsFilterRegistration(CorsPropertiesList corsPropertiesList) {
            System.out.println("create bean FilterRegistrationBean with " + corsPropertiesList);
            FilterRegistrationBean bean = new FilterRegistrationBean(createCorsFilter(corsPropertiesList));
            bean.setOrder(0);
            return bean;
        }
    }

    /**
     * 不可与 @CrossOrigin 联用
     */
    @Configuration
    @ConditionalOnProperty(prefix = "web.config", name = "cors", havingValue = "corsFilter")
    public static class CorsFilterConfig {

        @Bean(name = "corsFilter")
        public CorsFilter corsFilter(CorsPropertiesList corsPropertiesList) {
            System.out.println("init bean CorsFilter with " + corsPropertiesList);
            return createCorsFilter(corsPropertiesList);
        }
    }

    private static CorsFilter createCorsFilter(CorsPropertiesList corsPropertiesList) {
        return new CorsFilter(createCorsConfigurationSource(corsPropertiesList));
    }

    private static CorsConfigurationSource createCorsConfigurationSource(CorsPropertiesList corsPropertiesList) {
        UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();

        for(CorsProperties corsProperties: corsPropertiesList.getList()) {
            // 路径也是 AntPathMarcher
            for(String pathPattern: corsProperties.getPathPatterns()) {
                source.registerCorsConfiguration(pathPattern, toCorsConfiguration(corsProperties));
            }
        }
        return source;
    }

    private static CorsConfiguration toCorsConfiguration(CorsProperties corsProperties) {
        CorsConfiguration corsConfig = new AntPathMatcherCorsConfiguration();
        corsConfig.setAllowedOrigins(Arrays.asList(corsProperties.getAllowedOrigins()));
        corsConfig.setAllowedMethods(Arrays.asList(corsProperties.getAllowedMethods()));
        corsConfig.setAllowedHeaders(Arrays.asList(corsProperties.getAllowedHeaders()));
        corsConfig.setAllowCredentials(corsProperties.getAllowedCredentials());
        corsConfig.setMaxAge(corsProperties.getMaxAge());
        corsConfig.setExposedHeaders(Arrays.asList(corsProperties.getExposedHeaders()));

        return corsConfig;
    }

    /**
     * 可与 @CrossOrigin 联用
     */
    @Configuration
    @ConditionalOnProperty(prefix = "web.config", name = "cors", havingValue = "customFilter")
    public static class CustomCorsFilterConfig {

        @Bean
        public CustomCorsFilter customCorsFilter(CorsPropertiesList corsPropertiesList) {
            System.out.println("init bean CustomCorsFilter with " + corsPropertiesList);
            return new CustomCorsFilter(corsPropertiesList);
        }
    }

    /**
     * 可与 @CrossOrigin 联用
     */
    @Configuration
    @EnableWebMvc
    @ConditionalOnProperty(prefix = "web.config", name = "cors", havingValue = "webMvc")
    public class WebConfig implements WebMvcConfigurer {
        //
        // @Override
        // public void addCorsMappings(CorsRegistry registry) {
        //
        //     registry.addMapping("/api/**")
        //         .allowedOrigins("https://domain2.com")
        //         .allowedMethods("PUT", "DELETE")
        //         .allowedHeaders("header1", "header2", "header3")
        //         .exposedHeaders("header1", "header2")
        //         .allowCredentials(true).maxAge(3600);
        //
        //     // Add more mappings...
        // }

        @Autowired
        private CorsPropertiesList corsPropertiesList;

        @Override
        public void addCorsMappings(CorsRegistry registry) {
            System.out.println("config cors with " + corsPropertiesList.toString());
            for(CorsProperties corsProperties: corsPropertiesList.getList()) {
                addCorsMappings(registry, corsProperties);
            }
        }

        private void addCorsMappings(CorsRegistry registry, CorsProperties corsProperties) {
            for(String pathPattern: corsProperties.getPathPatterns()) {
                CorsRegistration registration = registry.addMapping(pathPattern);
                registration.allowedOrigins(corsProperties.getAllowedOrigins());
                registration.allowedMethods(corsProperties.getAllowedMethods());
                registration.allowedHeaders(corsProperties.getAllowedHeaders());
                registration.allowCredentials(corsProperties.getAllowedCredentials());
                registration.exposedHeaders(corsProperties.getExposedHeaders());
                registration.maxAge(corsProperties.getMaxAge());
            }
        }
    }
}