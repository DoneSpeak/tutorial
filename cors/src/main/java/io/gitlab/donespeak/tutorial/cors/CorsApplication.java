package io.gitlab.donespeak.tutorial.cors;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;

@EnableConfigurationProperties
@SpringBootApplication
public class CorsApplication {

    public static void main(String[] args) {
        SpringApplication.run(CorsApplication.class, args);
    }

}
