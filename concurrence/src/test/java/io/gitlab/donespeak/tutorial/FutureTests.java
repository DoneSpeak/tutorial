package io.gitlab.donespeak.tutorial;

import com.google.common.util.concurrent.ListeningExecutorService;
import com.google.common.util.concurrent.MoreExecutors;
import org.junit.BeforeClass;
import org.junit.Test;

import java.time.Duration;
import java.util.concurrent.CancellationException;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.IntStream;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

/**
 * @author donespeak
 */
public class Futures {

    private static ExecutorService executorService;

    @BeforeClass
    public static void setUp() {
        executorService = Executors.newFixedThreadPool(100);
    }

    private Future<String> submitFuture(Runnable innerRun, Duration sleep, Class<? extends Exception> exception) {
        Future<String> future = executorService.submit(() -> {
            if (innerRun != null) {
                innerRun.run();
            }
            if (sleep != null) {
                try {
                    Thread.sleep(sleep.toMillis());
                } catch (InterruptedException e) {
                    // do nothing
                }
            }
            if (exception != null) {
                throw exception.newInstance();
            }
            return "OK";
        });
        return future;
    }

    @Test
    public void getAfterTimeoutShouldWork() throws ExecutionException, InterruptedException {
        Future<String> future = submitFuture(null, Duration.ofSeconds(3), null);
        IntStream.range(0, 2).forEach(i -> {
            try {
                future.get(1, TimeUnit.SECONDS);
                fail();
            } catch (Exception e) {
                // do nothing
                assertTrue(TimeoutException.class.isInstance(e));
            }
        });
        assertFalse(future.isCancelled());
        assertFalse(future.isDone());

        String result = future.get();
        assertEquals("OK", result);
    }

    @Test
    public void getAfterCancelShouldFail() throws ExecutionException, InterruptedException {
        Future<String> future = submitFuture(null, Duration.ofSeconds(2), null);
        assertTrue(future.cancel(true));
        assertTrue(future.isCancelled());
        assertTrue(future.isDone());
        try {
            future.get();
        } catch (Exception e) {
            assertTrue(CancellationException.class.isInstance(e));
        }
    }

    @Test
    public void getAfterExceptionShouldFail()  {
        AtomicInteger count = new AtomicInteger(0);
        Future<String> future = submitFuture(() -> count.incrementAndGet(), Duration.ofSeconds(2), TimeoutException.class);
        assertFalse(future.isCancelled());
        assertFalse(future.isDone());
        try {
            future.get();
        } catch (Exception e) {
            // future 执行过程中抛出的异常均会被包装为 ExecutionException
            assertTrue(ExecutionException.class.isInstance(e));
            // e.getCause() 为实际异常
            assertTrue(TimeoutException.class.isInstance(e.getCause()));
        }
        assertEquals(1, count.intValue());
        try {
            // 重新获取，抛出一样的异常
            future.get();
        } catch (Exception e) {
            assertTrue(ExecutionException.class.isInstance(e));
            assertTrue(TimeoutException.class.isInstance(e.getCause()));
        }
        // 重复获取不会重复执行
        assertEquals(1, count.intValue());
    }

    @Test
    public void throwExecutionExceptionIfInterruptedException()  {
        AtomicInteger count = new AtomicInteger(0);
        Future<String> future = submitFuture(() -> count.incrementAndGet(), Duration.ofSeconds(2), InterruptedException.class);
        try {
            future.get();
        } catch (Exception e) {
            // future 执行过程中抛出的异常均会被包装为 ExecutionException
            assertTrue(ExecutionException.class.isInstance(e));
            // e.getCause() 为实际异常
            assertTrue(InterruptedException.class.isInstance(e.getCause()));
        }
        assertFalse(future.isCancelled());
        assertTrue(future.isDone());
        assertEquals(1, count.intValue());
    }

    @Test
    public void throwExecutionExceptionIfCancellationException() {
        AtomicInteger count = new AtomicInteger(0);
        Future<String> future = submitFuture(() -> count.incrementAndGet(), Duration.ofSeconds(2), CancellationException.class);
        try {
            future.get();
        } catch (Exception e) {
            // future 执行过程中抛出的异常均会被包装为 ExecutionException
            assertTrue(ExecutionException.class.isInstance(e));
            // e.getCause() 为实际异常
            assertTrue(CancellationException.class.isInstance(e.getCause()));
        }
        assertFalse(future.isCancelled());
        assertTrue(future.isDone());
        assertEquals(1, count.intValue());
    }

    @Test
    public void tr() throws ExecutionException, InterruptedException {
        Future<String> future = submitFuture(() -> {
            Thread.interrupted();
        }, Duration.ofSeconds(2), null);
        try {
            future.get();
        } catch (Exception e) {
            e.printStackTrace();
            assertTrue(CancellationException.class.isInstance(e));
        }
    }

    @Test
    public void throwInCallback() {
        ListeningExecutorService executorService = MoreExecutors.listeningDecorator(Executors.newCachedThreadPool());
        var future = executorService.submit(() -> {
           return "result value";
        });

    }

}
