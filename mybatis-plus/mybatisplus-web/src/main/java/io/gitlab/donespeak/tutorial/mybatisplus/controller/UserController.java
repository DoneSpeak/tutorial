package io.gitlab.donespeak.tutorial.mybatisplus.controller;

import io.gitlab.donespeak.tutorial.mybatisplus.entity.User;
import io.gitlab.donespeak.tutorial.mybatisplus.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @author Yang Guanrong
 * @date 2019/11/18 18:30
 */
@RestController
@RequestMapping("/users")
public class UserController {

    @Autowired
    private UserService userService;

    @GetMapping("")
    public List<User> listUsers() {
        return userService.list();
    }

    @GetMapping("/{userId}")
    public User listUsers(@PathVariable long userId) {
        return userService.getById(userId);
    }
}