package io.gitlab.donespeak.tutorial.pdf.util;

import com.itextpdf.text.pdf.parser.PdfImageObject;
import org.apache.pdfbox.pdmodel.graphics.image.PDImageXObject;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

/**
 * @author Yang Guanrong
 */
public class ImageUtil {

    public static void toFile(byte[] bytes, String formatname, File file) throws IOException {
        // TODO
    }

    public static void toFile(BufferedImage image, String formatname, File file) throws IOException {
        ImageIO.write(image, formatname, file);
    }

    public static void toFile(PDImageXObject image, File file) throws IOException {
        ImageIO.write(image.getImage(), image.getSuffix(), file);
    }

    public static void toFile(PdfImageObject image, File file) throws IOException {
        ImageIO.write(image.getBufferedImage(), image.getFileType(), file);
    }

}
