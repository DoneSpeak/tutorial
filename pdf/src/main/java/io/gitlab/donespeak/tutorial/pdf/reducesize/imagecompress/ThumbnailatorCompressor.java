package io.gitlab.donespeak.tutorial.pdf.reducesize.imagecompress;

import net.coobird.thumbnailator.Thumbnails;

import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;

/**
 * https://github.com/coobird/thumbnailator/wiki/Examples
 *
 * @author Yang Guanrong
 */
public class ThumbnailatorCompressor implements ImageCompressor {

    @Override
    public BufferedImage compress(BufferedImage image, String imageFormat, double quality, double scale) throws IOException {
        System.out.println("ThumbnailatorCompressor#type: " + image.getType());
        // int imageType = "png".equalsIgnoreCase(imageFormat)? BufferedImage.TYPE_INT_ARGB: image.getType();
        BufferedImage thumbnail = Thumbnails.of(image)
            .imageType(image.getType())
            .scale(scale)
            .outputQuality(quality)
            // .outputFormat(imageFormat)
            .useOriginalFormat()
            .asBufferedImage();

        return thumbnail;
    }

    @Override
    public OutputStream compressToStream(BufferedImage image, String imageFormat, double quality, double scale) throws IOException {
        OutputStream os = new ByteArrayOutputStream();
        System.out.println("ThumbnailatorCompressor#type: " + image.getType());
        // int imageType = "png".equalsIgnoreCase(imageFormat)? BufferedImage.TYPE_INT_ARGB: image.getType();
        Thumbnails.of(image)
            .imageType(image.getType())
            .scale(scale)
            // .outputFormat(imageFormat)
            .useOriginalFormat()
            .outputQuality(quality)
            .toOutputStream(os);

        return os;
    }
}
