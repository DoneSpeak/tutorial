package io.gitlab.donespeak.tutorial.pdf.itext5;

import com.itextpdf.text.Document;
import com.itextpdf.text.exceptions.InvalidPdfException;
import com.itextpdf.text.pdf.PdfReader;

import java.io.File;
import java.io.IOException;

/**
 * @author donespeak
 */
public class CorruptedJudgement {

    public static boolean isCorrupted(File pdf) throws IOException {
        boolean isCorrupted = false;
        try {
            Document document = new Document(new PdfReader(pdf.getAbsolutePath()).getPageSize(1));
            document.open();
            PdfReader reader = new PdfReader(pdf.getAbsolutePath());
            int n = reader.getNumberOfPages();
            if (n != 0) {
                isCorrupted = true;
            }
            document.close();
            return isCorrupted;
        } catch (InvalidPdfException ex) {
            return true;
        }
    }
}
