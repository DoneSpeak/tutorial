package io.gitlab.donespeak.tutorial.pdf.reducesize.itext;

import com.itextpdf.text.DocumentException;
import com.itextpdf.text.pdf.PdfReader;
import com.itextpdf.text.pdf.PdfStamper;
import com.itextpdf.text.pdf.PdfStream;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

/**
 * 参考：https://gist.github.com/sween/ef7e6fea295a9a27fdd1
 *
 * @author Yang Guanrong
 */
public class ItextReduceSize {

    /**
     * 没有效果。
     *
     * 需要理解 setFullCompression 做了什么才能理解压缩是怎么做的
     */
    public static void reduce(File input, File output) throws IOException, DocumentException {
        PdfReader reader = new PdfReader(new FileInputStream(input));
        PdfStamper stamper = new PdfStamper(reader, new FileOutputStream(output));
        try {
            //PdfWriter writer = stamper.wr
            reader.removeFields();
            // 移除没有使用的对象
            reader.removeUnusedObjects();

            int total = reader.getNumberOfPages() + 1;
            for (int i = 1; i < total; i++) {
                reader.setPageContent(i + 1, reader.getPageContent(i + 1));
            }
            stamper.setFullCompression();
            stamper.getWriter().setCompressionLevel(PdfStream.DEFAULT_COMPRESSION);
            // stamper.getWriter().setFullCompression();
        } finally {
            if(stamper != null) {
                stamper.close();
            }
        }
    }
}
