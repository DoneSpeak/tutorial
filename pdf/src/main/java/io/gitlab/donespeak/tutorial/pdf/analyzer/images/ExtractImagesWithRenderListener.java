package io.gitlab.donespeak.tutorial.pdf.analyzer.images;

import com.itextpdf.text.pdf.PdfName;
import com.itextpdf.text.pdf.PdfReader;
import com.itextpdf.text.pdf.parser.ImageRenderInfo;
import com.itextpdf.text.pdf.parser.PdfImageObject;
import com.itextpdf.text.pdf.parser.PdfReaderContentParser;
import io.gitlab.donespeak.tutorial.pdf.core.DefaultRenderListener;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;

/**
 * @author Yang Guanrong
 */
public class ExtractImagesWithRenderListener implements ImageExtract {

    @Override
    public void extract(File pdf, File imageDir) throws IOException {
        extract(new FileInputStream(pdf), imageDir);
    }

    /**
     * 实现来源：
     * https://github.com/mkl-public/testarea-itext5/blob/master/src/test/java/mkl/testarea/itext5/extract/ImageExtraction.java
     *
     * @param pdf
     * @param imageDir
     * @throws IOException
     */
    @Override
    public void extract(InputStream pdf, File imageDir) throws IOException {
        if (!imageDir.exists()) {
            imageDir.mkdirs();
        }
        PdfReader reader = new PdfReader(pdf);
        for (int page = 1; page <= reader.getNumberOfPages(); page++) {
            PdfReaderContentParser parser = new PdfReaderContentParser(reader);
            final int pageIndex = page;
            parser.processContent(page, new DefaultRenderListener() {

                @Override
                public void renderImage(ImageRenderInfo renderInfo) {
                    try {
                        byte[] bytes = renderInfo.getImage().getImageAsBytes();
                        PdfImageObject.ImageBytesType type = renderInfo.getImage().getImageBytesType();
                        // TODO PNG 图片背景黑化
                        if (bytes != null && type != null) {
                            Files.write(
                                new File(imageDir, pageIndex + "-" + System.nanoTime() + "." + type.getFileExtension())
                                    .toPath(),
                                bytes);
                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            });
        }
    }

    private String getImageExtension(PdfImageObject image) {
        PdfName filter = (PdfName)image.get(PdfName.FILTER);

        if (PdfName.DCTDECODE.equals(filter)) {
            return PdfImageObject.ImageBytesType.JPG.getFileExtension();
        } else if (PdfName.JPXDECODE.equals(filter)) {
            return PdfImageObject.ImageBytesType.JP2.getFileExtension();
        } else if (PdfName.FLATEDECODE.equals(filter)) {
            return PdfImageObject.ImageBytesType.PNG.getFileExtension();
        } else if (PdfName.LZWDECODE.equals(filter)) {
            return PdfImageObject.ImageBytesType.CCITT.getFileExtension();
        }
        return "";
    }
}
