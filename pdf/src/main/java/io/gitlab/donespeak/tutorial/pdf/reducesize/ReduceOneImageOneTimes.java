package io.gitlab.donespeak.tutorial.pdf.reducesize;

import io.gitlab.donespeak.tutorial.pdf.reducesize.imagecompress.ThumbnailatorCompressor;
import org.apache.pdfbox.cos.COSDocument;
import org.apache.pdfbox.cos.COSName;
import org.apache.pdfbox.cos.COSObject;
import org.apache.pdfbox.cos.COSObjectKey;
import org.apache.pdfbox.cos.COSStream;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.graphics.PDXObject;
import org.apache.pdfbox.pdmodel.graphics.image.PDImageXObject;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Iterator;
import java.util.Map;

/**
 * @author Yang Guanrong
 */
public class ReduceOneImageOneTimes {

    // private static File pdf = new File("pdf/asset/horse_relaxed.pdf");
    private static File pdf = new File("pdf/asset/horse_releaxed_reuseimage_20.pdf");
    private static File pdfCompressed = new File("pdf/target/output/" + ReduceOneImageOneTimes.class.getSimpleName(), fileName(pdf) + "-compressed.pdf");
    private static File resultDir = new File("pdf/target/output/" + ReduceOneImageOneTimes.class.getSimpleName() + "/");

    private static ThumbnailatorCompressor compressor = new ThumbnailatorCompressor();

    public static void main(String[] args) throws IOException {
        // compress(pdf, pdfCompressed);
        PDDocument doc = tryParse(pdf);

        doc.save(new FileOutputStream(pdfCompressed));
        doc.close();
    }

    private static PDDocument tryParse(File pdf) throws IOException {

        PDDocument doc = PDDocument.load(pdf);
        COSDocument cDoc = doc.getDocument();
        for(COSObject cosObject: cDoc.getObjects()) {
            System.out.println(cosObject.getObject());
            if (!COSName.IMAGE.equals(cosObject.getDictionaryObject(COSName.SUBTYPE))) {
                continue;
            }
            COSStream stream = (COSStream)cosObject.getObject();

            PDImageXObject pdi = (PDImageXObject) PDXObject.createXObject(cosObject.getObject(), null);
            BufferedImage bufferedImage = compressor.compress(pdi.getImage(), pdi.getSuffix(), 0.5, 0.5);

            // try ( OutputStream outputStream = stream.createOutputStream(cosObject.getDictionaryObject(COSName.FILTER))) {
            //     ImageIO.write(bufferedImage, pdi.getSuffix(), outputStream);
            // }
        }
        return doc;
    }

    private static void process(COSObject cosObject, PDDocument doc) throws IOException {

        PDImageXObject pdi = (PDImageXObject) PDXObject.createXObject(cosObject.getObject(), null);

        BufferedImage bufferedImage = compressor.compress(pdi.getImage(), pdi.getSuffix(), 0.5, 0.5);

        ByteArrayOutputStream imgBytes = new ByteArrayOutputStream();
        ImageIO.write(bufferedImage, pdi.getSuffix(), imgBytes);
        PDImageXObject newPdi = PDImageXObject.createFromByteArray(doc, imgBytes.toByteArray(), null);
    }

    private static String fileName(File file) {
        return file.getName().substring(0, file.getName().lastIndexOf("."));
    }

    public static void compress(File input, File output) throws IOException {
        if (!output.getParentFile().exists()) {
            output.getParentFile().mkdirs();
        }
        PDDocument doc = PDDocument.load(input);
        Iterator<Map.Entry<COSObjectKey, Long>> xrefEntriesIt = doc.getDocument().getXrefTable().entrySet().iterator();
        while (xrefEntriesIt.hasNext()) {
            COSObject object = doc.getDocument().getObjectFromPool(xrefEntriesIt.next().getKey());
            System.out.println(object.getObjectNumber() + " " + object.getGenerationNumber());
            if (COSName.IMAGE.equals(object.getDictionaryObject(COSName.SUBTYPE))) {
                changeImage(object, doc);
            }
        }
        doc.save(new FileOutputStream(output));
        doc.close();
    }

    private static void changeImage(COSObject obj, PDDocument doc) throws IOException {
        System.out.println(obj);
        PDImageXObject pdi = (PDImageXObject) PDXObject.createXObject(obj.getObject(), null);
        BufferedImage bufferedImage = compressor.compress(pdi.getImage(), pdi.getSuffix(), 0.5, 0.5);

        ByteArrayOutputStream imgBytes = new ByteArrayOutputStream();
        ImageIO.write(bufferedImage, pdi.getSuffix(), imgBytes);
        PDImageXObject newPdi = PDImageXObject.createFromByteArray(doc, imgBytes.toByteArray(), null);
    }
}
