package io.gitlab.donespeak.tutorial.pdf.reducesize.imagecompress;

import java.awt.image.BufferedImage;
import java.io.IOException;
import java.io.OutputStream;

/**
 * @author Yang Guanrong
 */
public interface ImageCompressor {

    BufferedImage compress(BufferedImage image, String imageFormat, double quality, double scala) throws IOException;

    OutputStream compressToStream(BufferedImage image, String imageFormat, double quality, double scala) throws IOException;
}
