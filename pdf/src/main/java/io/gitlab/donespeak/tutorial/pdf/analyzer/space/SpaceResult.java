package io.gitlab.donespeak.tutorial.pdf.analyzer.space;

import lombok.Data;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Yang Guanrong
 */
@Data
public class SpaceResult {
    private List<Space> items = new ArrayList<>();
    private int total;
}
