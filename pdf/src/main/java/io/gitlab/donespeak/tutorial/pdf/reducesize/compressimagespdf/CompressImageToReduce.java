package io.gitlab.donespeak.tutorial.pdf.reducesize.compressimagespdf;

import org.apache.pdfbox.cos.COSName;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.PDPageTree;
import org.apache.pdfbox.pdmodel.PDResources;
import org.apache.pdfbox.pdmodel.common.PDStream;
import org.apache.pdfbox.pdmodel.graphics.PDXObject;
import org.apache.pdfbox.pdmodel.graphics.image.PDImageXObject;

import java.io.File;
import java.io.IOException;

/**
 * @author Yang Guanrong
 */
public class CompressImageToReduce {

    public void reduceSize(File pdf, File imageDir) throws IOException {
        if(!imageDir.exists()) {
            imageDir.mkdirs();
        }
        PDDocument document = PDDocument.load(pdf);
        PDPageTree list = document.getPages();
        for (PDPage page : list) {
            PDResources pdResources = page.getResources();

            for (COSName c : pdResources.getXObjectNames()) {
                PDXObject o = pdResources.getXObject(c);
                if (o instanceof PDImageXObject) {
                    PDImageXObject img = (PDImageXObject) o;
                    PDStream stream = img.getStream();
                }
            }
        }
    }

}
