package io.gitlab.donespeak.tutorial.pdf.util;

import java.io.File;

/**
 * @author Yang Guanrong
 */
public class FileAssistant {

    public static String getExt(String filename) {
        int lastDot = filename.lastIndexOf(".");
        if(lastDot < 0) {
            return "";
        }
        return filename.substring(lastDot + 1);
    }

    public static String getExt(File file) {
        return getExt(file.getName());
    }

    public static String getFileName(String filename) {
        int lastDot = filename.lastIndexOf(".");
        int lastSlash = filename.lastIndexOf(File.separator) + 1;
        lastDot = lastDot < 0? filename.length(): lastDot;
        return filename.substring(lastSlash, lastDot);
    }

    public static String getFileName(File file) {
        return getFileName(file.getName());
    }

    public static void createDirIfNonExist(File dir) {
        if(!dir.exists()) {
            dir.mkdirs();
        }
    }

    public static void createParentDirIfNonExist(File dir) {
        if (!dir.getParentFile()
            .exists()) {
            dir.getParentFile()
                .mkdirs();
        }
    }

    public static void removeIfExist(File file) {
        if(!file.exists()) {
            return;
        }
        if(file.isDirectory()) {
            for(File f: file.listFiles()) {
                removeIfExist(f);
            }
        }
        file.delete();
    }

    public static void main(String[] args) {
        removeIfExist(new File("/Users/yangguanrong/Codes/Gitlab/tutorial/pdf/target/output/ReducePdfSizeByCompressJpegTest/replaced-1.0-0.5/ReducePdfSizeByCompressJpeg"));
    }
}
