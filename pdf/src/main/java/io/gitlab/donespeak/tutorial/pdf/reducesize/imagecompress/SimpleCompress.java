package io.gitlab.donespeak.tutorial.pdf.reducesize.imagecompress;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.geom.AffineTransform;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;

/**
 * @author Yang Guanrong
 */
public class SimpleCompress implements ImageCompressor {

    @Override
    public BufferedImage compress(BufferedImage image, String imageFormat, double quality, double scale) throws IOException {
        int width = (int)(image.getWidth() * scale);
        int height = (int)(image.getHeight() * scale);
        int imageType = BufferedImage.TYPE_INT_RGB;
        // 缩放图片
        BufferedImage img = new BufferedImage(width, height, image.getType());
        AffineTransform at = AffineTransform.getScaleInstance(scale, scale);
        Graphics2D g = img.createGraphics();
        g.setComposite(AlphaComposite.getInstance(AlphaComposite.SRC_ATOP, 1.0f));  // 1.0f为透明度 ，值从0-1.0，依次变得不透明
        g.drawRenderedImage(image, at);
        return img;
    }

    @Override
    public OutputStream compressToStream(BufferedImage image, String imageFormat, double quality, double scale) throws IOException {
        ByteArrayOutputStream imgBytes = new ByteArrayOutputStream();
        // TODO 不同类型的图片应该做不同类型的处理
        BufferedImage img = compress(image, imageFormat, quality, scale);
        ImageIO.write(img, "JPG", imgBytes);
        return imgBytes;
    }
}
