PDF Reduce Size
===

## 工具网站

- https://pdfresizer.com/optimize
- https://www.pdf2go.com/zh/compress-pdf
- https://www.ilovepdf.com/zh-cn/compress_pdf
- https://www.99pdf.com/ 看起来专业版效果会很好(6.9MB -> 5.45/1.62/0.88MB 清晰度相同)
- https://smallpdf.com/compress-pdf 6.9MB -> 1.4MB

## PDF成分分析

### 文档结构

```java
+--------------------------+
| +----------------------+ |
| |        Header        | |  <-----文件头，表示版本.%PDF-1.M
| |                      | |
| +----------------------+ |
| |                      | |
| |         Body         | |  <-----文件体，由一系列PDF对象组成
| |                      | |
| |                      | |
| |                      | |
| |                      | |
| |                      | |
| +----------------------+ |
| |    Cross-reference   | |  <-----交叉引用表，包含指向所有间接
| |         table        | |        对象的文件位置索引的列表
| |        (xref)        | |
| +----------------------+ |
| |        Trailer       | |  <-----包含文件的根节点信息和
| |                      | |        文件解析的起点信息
| +----------------------+ |
+--------------------------+
```

```
%PDF-1.7

1 0 obj
<<
 /Type /Catalog
 /Outlines 2 0 R
 /Pages 3 0 R
>>
endobj

2 0 obj
<<
 /Type /Outlines
 /Count 0
>>
endobj

3 0 obj
<<
 /Type /Pages
 /Kids [4 0 R]
 /Count 1
>>
endobj

4 0 obj
<<
 /Type /Page
 /Parent 3 0 R
 /MediaBox [0 0 612 792]
 /Contents 5 0 R
 /Resources
 << /ProcSet 6 0 R
    /Font << /F1 7 0 R >>
 >>
>>
endobj

5 0 obj
<< /Length 48 >>
stream
BT
/F1 24 Tf
100 700 Td
(Hello World)Tj
ET
endstream
endobj

6 0 obj
[/PDF /Text]
endobj

7 0 obj
<<
 /Type /Font
 /Subtype /Type1
 /Name /F1
 /BaseFont /Helvetica
 /Encoding /MacRomanEncoding
>>
endobj

xref
0 8
0000000000 65535 f
0000000012 00000 n
0000000089 00000 n
0000000145 00000 n
0000000214 00000 n
0000000381 00000 n
0000000485 00000 n
0000000518 00000 n
trailer
<<
 /Size 8
 /Root 1 0 R
>>
startxref
642
%%EOF
```

- [PDF 文档结构](https://lazymind.me/2017/10/pdf-structure/)
- [Anatomy of a PDF document](https://amccormack.net/2012-01-22-anatomy-of-a-pdf-document.html)

### 工具分析

### Java代码分析

## 可以考虑的压缩方向

压缩的目的：

- 屏幕观看（兼容不同的应用打开）
- 输出印刷

压缩方向：

- 字体
- 图片

压缩方法：

- 位图部分除了分辨率问题外，还存在一个在PDF文件内部的压缩方法问题：可以采用 JPG 压缩，TIF 压缩 或者ZIP 压缩方法 来保存内嵌的位图。其中JPG 压缩是有损的，但体积上最优势，TIF 压缩率为 1/2， Zip压缩就最弱，但兼容性最好，是印刷输出的最佳选择。
- 关于矢量部分的压缩，个人建议不要使用，因为这个选项会缩减你的曲线节点，在小磅数的文字曲线时，O字母都看起来是六角形。
- 关于文件大小的另外一个选项就是：是否将所有文字转换为曲线？。这个通常都建议转换，但头疼的是：如果你使用一些劣质的中文字体，就会造成矢量描述有误的PDF文件，结果就是PDF部分缺失……你后悔都来不及。

## Java实现方法
