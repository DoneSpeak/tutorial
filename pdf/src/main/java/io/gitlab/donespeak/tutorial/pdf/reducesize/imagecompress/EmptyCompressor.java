package io.gitlab.donespeak.tutorial.pdf.reducesize.imagecompress;

import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;

/**
 * @author Yang Guanrong
 */
public class EmptyCompressor implements ImageCompressor {

    @Override
    public BufferedImage compress(BufferedImage image, String imageFormat, double quality, double scala) throws IOException {
        int num = Math.min(image.getWidth(), image.getHeight());

        BufferedImage bufferedImage = new BufferedImage(image.getWidth() / num, image.getHeight() / num, image.getType());
        return bufferedImage;
    }

    @Override
    public OutputStream compressToStream(BufferedImage image, String imageFormat, double quality, double scala) throws IOException {
        return new ByteArrayOutputStream();
    }
}
