package io.gitlab.donespeak.tutorial.pdf.analyzer.images;

import com.itextpdf.text.pdf.PRStream;
import com.itextpdf.text.pdf.PdfName;
import com.itextpdf.text.pdf.PdfObject;
import com.itextpdf.text.pdf.PdfReader;
import com.itextpdf.text.pdf.parser.PdfImageObject;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Arrays;
import java.util.List;

/**
 * @author Yang Guanrong
 */
public class ExtractImagesReplaceHightResolutionImage implements ImageExtract {

    @Override
    public void extract(File pdf, File imageDir) throws IOException {
        if(!imageDir.exists()) {
            imageDir.mkdirs();
        }
        PdfReader reader = new PdfReader(new FileInputStream(pdf));
        int n = reader.getXrefSize();
        PdfObject object;

        for (int i = 0; i < n; i++) {

            object = reader.getPdfObject(i);
            List<PRStream> streams = findImageStream(object);
            if (streams.isEmpty()) {
                continue;
            }
            for(PRStream stream: streams) {
                PdfImageObject pdfImageObject = new PdfImageObject(stream);
                BufferedImage bi = pdfImageObject.getBufferedImage();
                if (bi == null) {
                    continue;
                }
                PdfImageObject.ImageBytesType type = pdfImageObject.getImageBytesType();
                if (type != null) {
                    if(PdfImageObject.ImageBytesType.PNG.equals(type)) {
                        System.out.println("BufferedImage#type:" + bi.getType());
                    }
                    File image = new File(imageDir, i + "-" + System.nanoTime() + "." + type.getFileExtension());
                    ImageIO.write(bi, type.getFileExtension(), image);
                }
            }
        }
    }

    private List<PRStream> findImageStream(PdfObject object) {
        PRStream stream;
        if (object == null || !object.isStream()) {
            // 判断是否为流
            return Arrays.asList();
        }
        stream = (PRStream)object;
        System.out.println(stream.getAsName(PdfName.SUBTYPE));
        if (!PdfName.IMAGE.equals(stream.getAsName(PdfName.SUBTYPE))) {
            // 判断是否为图片
            return Arrays.asList();
        }
        PdfName pdfName = stream.getAsName(PdfName.FILTER);
        if (!PdfName.DCTDECODE.equals(pdfName) && !PdfName.FLATEDECODE.equals(pdfName)) {
            // TODO 研究 DCTDECODE 是什么文件 -> JPG
            return Arrays.asList();
        }
        // if (PdfName.DCTDECODE.equals(filter)) {
        //     return PdfImageObject.ImageBytesType.JPG.getFileExtension();
        // } else if (PdfName.JPXDECODE.equals(filter)) {
        //     return PdfImageObject.ImageBytesType.JP2.getFileExtension();
        // } else if (PdfName.FLATEDECODE.equals(filter)) {
        //     return PdfImageObject.ImageBytesType.PNG.getFileExtension();
        // } else if (PdfName.LZWDECODE.equals(filter)) {
        //     return PdfImageObject.ImageBytesType.CCITT.getFileExtension();
        // }

        return Arrays.asList(stream);
    }

    @Override
    public void extract(InputStream pdf, File imageDir) throws IOException {

    }
}
