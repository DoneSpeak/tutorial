package io.gitlab.donespeak.tutorial.pdf.analyzer;

import io.gitlab.donespeak.tutorial.pdf.analyzer.images.ExtractImagesWithRenderListener;
import org.junit.Test;

import java.io.File;
import java.io.IOException;

public class ExtractImagesWithRenderListenerTest {

    @Test
    public void extract() throws IOException {

        File pdf = new File("/Users/yangguanrong/Downloads/testpdf/report-replaced-0.01.pdf");
        File imageDir = new File("/Users/yangguanrong/Downloads/testpdf/report-replaced-0.01/images/");
        ExtractImagesWithRenderListener extractor = new ExtractImagesWithRenderListener();
        extractor.extract(pdf, imageDir);
    }
}