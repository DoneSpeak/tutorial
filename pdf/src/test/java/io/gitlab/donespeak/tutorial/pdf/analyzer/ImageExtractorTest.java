package io.gitlab.donespeak.tutorial.pdf.analyzer;

import io.gitlab.donespeak.tutorial.pdf.analyzer.images.ImageExtractor;

import java.io.File;
import java.io.IOException;

public class ImageExtractorTest {

    public static void main(String[] args) throws IOException {

        File pdf = new File("/Users/yangguanrong/Downloads/testpdf/compress-by-other/original.pdf");
        File imageDir = new File("/Users/yangguanrong/Downloads/testpdf/compress-by-other/original/images/" + ImageExtractor.class.getSimpleName());
        new ImageExtractor().extract(pdf, imageDir);
    }
}