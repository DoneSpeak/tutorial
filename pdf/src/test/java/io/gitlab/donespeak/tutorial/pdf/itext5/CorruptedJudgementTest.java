package io.gitlab.donespeak.tutorial.pdf.itext5;

import org.junit.Test;

import java.io.File;
import java.io.IOException;

/**
 * @author donespeak
 */
public class CorruptedJudgementTest {

    @Test
    public void test() throws IOException {
        File file = new File("/Users/guanrongyang/Downloads/Property Image Report - 718 W 13TH ST, LAROSE, LA.pdf");
        boolean result = CorruptedJudgement.isCorrupted(file);
        System.out.println(result);

        file = new File("/Users/guanrongyang/Desktop/062c1bd6be97bb6d063babfb140b941e.pdf");
        result = CorruptedJudgement.isCorrupted(file);
        System.out.println(result);
    }
}
