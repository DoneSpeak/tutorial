package io.gitlab.donespeak.tutorial.pdf.reducesize.itext;

import com.itextpdf.text.DocumentException;
import io.gitlab.donespeak.tutorial.pdf.reducesize.util.CommonUtil;

import java.io.File;
import java.io.IOException;

public class ItextReduceSizeTest {

    public static void main(String[] args) throws IOException, DocumentException {
        File input = new File("/Users/yangguanrong/Downloads/testpdf/report-replaced-0.0.pdf");
        File output = CommonUtil.getCompressName(input, "reduced");
        ItextReduceSize.reduce(input, output);
    }
}