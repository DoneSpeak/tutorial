package io.gitlab.donespeak.tutorial.stripetutorial;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class StripeTutorialApplication {

    public static void main(String[] args) {
        SpringApplication.run(StripeTutorialApplication.class, args);
    }

}
